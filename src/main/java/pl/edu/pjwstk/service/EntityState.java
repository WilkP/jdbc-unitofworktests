package pl.edu.pjwstk.service;

/**
 * Created by vpnk on 19.11.16.
 */
public enum EntityState {
    New, Changed, Unchanged, Deleted
}
