package pl.edu.pjwstk.db;

import pl.edu.pjwstk.service.OrderOrderItemMapping;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IOrderOrderItemMappingRepository extends IRepository<OrderOrderItemMapping> {

    public List<OrderOrderItemMapping> withOrderID(long orderID);
    public List<OrderOrderItemMapping> withOrderItemID(long orderItemID);
}
