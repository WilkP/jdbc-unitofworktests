package pl.edu.pjwstk.db;

import pl.edu.pjwstk.UnitOfWork.IUnitOfWork;
import pl.edu.pjwstk.UnitOfWork.IUnitOfWorkRepository;
import pl.edu.pjwstk.UnitOfWork.UnitOfWork;
import pl.edu.pjwstk.db.repos.IEntityBuilder;
import pl.edu.pjwstk.service.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vpnk on 22.11.16.
 */
public abstract class Repository<TEntity extends Entity> implements IRepository<TEntity>, IUnitOfWorkRepository {
    protected IUnitOfWork uow;
    protected Connection connection;
    protected PreparedStatement selectByID;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement selectAll;
    protected IEntityBuilder<TEntity> builder;

    protected String selectByIDSql = "SELECT * FROM " + getTableName() + "WHERE id = ?";
    protected String deleteSql = "DELETE FROM " + getTableName() + "WHERE id = ?";
    protected String selectAllSql = "SELECT * FROM " + getTableName();

    protected Repository(Connection connection, IEntityBuilder<TEntity> builder, UnitOfWork uow) {
        this.uow = uow;
        this.builder = builder;
        this.connection = connection;
        try {
            selectByID = connection.prepareStatement(selectByIDSql);
            delete = connection.prepareStatement(deleteSql);
            selectAll = connection.prepareStatement(selectAllSql);
            insert = connection.prepareStatement(getInsertQuery());
            update = connection.prepareStatement(getUpdateQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(TEntity entity) {
        uow.markAsNew(entity, this);
    }

    @Override
    public void update(TEntity entity) {
        uow.markAsDirty(entity, this);
    }

    @Override
    public void delete(TEntity entity) {
        uow.markAsDeleted(entity, this);
    }

    @Override
    public void persistAdd(Entity entity) {
        try {
            setUpInsertQuery((TEntity) entity);
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void persistUpdate(Entity entity){
        try{
            setUpUpdateQuery((TEntity) entity);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    @Override
    public void persistDelete(Entity entity){
        try{
            delete.setLong(1, entity.getId());
            delete.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    @Override
    public TEntity get(long id){
        try{
            selectByID.setLong(1, id);
            ResultSet rs = selectByID.executeQuery();
            while(rs.next()){
                return builder.build(rs);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public List<TEntity> getAll(){
        List<TEntity> result = new ArrayList<TEntity>();
        try{
            ResultSet rs = selectAll.executeQuery();
            while (rs.next()){
                result.add(builder.build(rs));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public abstract void setUpInsertQuery(TEntity entity) throws SQLException;
    public abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
    protected abstract String getTableName();
    protected abstract String getUpdateQuery();
    protected abstract String getInsertQuery();
}
