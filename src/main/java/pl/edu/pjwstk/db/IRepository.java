package pl.edu.pjwstk.db;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IRepository<TEntity> {

    public TEntity withId(long id);
    public void save(TEntity entity);
    public void update(TEntity entity);
    public void delete(TEntity entity);
    public TEntity get(long id);
    public List<TEntity> getAll();
}
