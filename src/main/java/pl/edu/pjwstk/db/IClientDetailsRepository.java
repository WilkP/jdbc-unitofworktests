package pl.edu.pjwstk.db;

import pl.edu.pjwstk.service.ClientDetails;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IClientDetailsRepository extends IRepository<ClientDetails> {
    public List<ClientDetails> withSurname(String surname);
    public List<ClientDetails> withName(String name);
    public List<ClientDetails> withLogin(String login);


}
