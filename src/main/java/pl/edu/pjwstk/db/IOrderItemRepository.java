package pl.edu.pjwstk.db;

import pl.edu.pjwstk.service.Order;
import pl.edu.pjwstk.service.OrderItem;

import java.util.List;

/**
 * Created by vpnk on 11.11.16.
 */
public interface IOrderItemRepository extends IRepository<OrderItem> {
    public List<OrderItem> withName(String name);
    public List<OrderItem> withDescription(String description);
    public List<OrderItem> withPrice(double price);
}
