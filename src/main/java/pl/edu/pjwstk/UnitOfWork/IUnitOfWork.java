package pl.edu.pjwstk.UnitOfWork;
import  pl.edu.pjwstk.service.Entity;

/**
 * Created by vpnk on 19.11.16.
 */
public interface IUnitOfWork {

    public void commit();
    public void rollback();
    public void markAsNew(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDirty(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
}
