package pl.edu.pjwstk;

import org.junit.Test;
import pl.edu.pjwstk.db.AddressManager;
import pl.edu.pjwstk.service.Address;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vpnk on 31.10.16.
 */
public class AddressManagerTest {

    AddressManager addressManager = new AddressManager();
    private final static String STREET_1 = "Śniadeckich";
    private final static String BUILDING_NUMBER_1 = "35";
    private final static String FLAT_NUMBER_1 = "61";
    private final static String POSTAL_CODE_1 = "80-808";
    private final static String CITY_1 = "Gdańsk";
    private final static String COUNTRY_1 = "Polska";
    @Test
    public void checkConnection(){
        assertNotNull(addressManager.getConnection());
    }

    @Test
    public void checkAdding(){
        Address address = new Address(STREET_1, BUILDING_NUMBER_1, FLAT_NUMBER_1, POSTAL_CODE_1, CITY_1, COUNTRY_1);

        addressManager.clearAddresses();
        assertEquals(1, addressManager.addAddress(address));

        List<Address> addressList = addressManager.getAllAddresses();
        Address receivedAddress = addressList.get(0);

        assertEquals(STREET_1, receivedAddress.getStreet());
        assertEquals(BUILDING_NUMBER_1, receivedAddress.getBuildingNumber());
        assertEquals(FLAT_NUMBER_1, receivedAddress.getFlatNumber());
        assertEquals(POSTAL_CODE_1, receivedAddress.getPostalCode());
        assertEquals(CITY_1, receivedAddress.getCity());
        assertEquals(COUNTRY_1, receivedAddress.getCountry());
    }

}