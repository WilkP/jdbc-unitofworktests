package pl.edu.pjwstk;

import org.junit.Test;
import pl.edu.pjwstk.db.ClientDetailsManager;
import pl.edu.pjwstk.service.ClientDetails;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vpnk on 31.10.16.
 */
public class ClientDetailsManagerTest {
    private final static String NAME_1 = "Paweł";
    private final static String SURNAME_1 = "Wilk";
    private final static String LOGIN_1 = "WilkP";

    ClientDetailsManager clientDetailsManager = new ClientDetailsManager();

    @Test
    public void checkConnection(){
        assertNotNull(clientDetailsManager.getConnection());
    }

    @Test
    public void checkAdding(){
        ClientDetails clientDetails = new ClientDetails(NAME_1, SURNAME_1, LOGIN_1);

        clientDetailsManager.clearClientDetails();

        assertEquals(1, clientDetailsManager.addClientDetails(clientDetails));

        List<ClientDetails> clientDetailsList= clientDetailsManager.getAllClientDetails();

        ClientDetails receivedClientDetails = clientDetailsList.get(0);

        assertEquals(NAME_1, receivedClientDetails.getName());
        assertEquals(SURNAME_1, receivedClientDetails.getSurname());
        assertEquals(LOGIN_1, receivedClientDetails.getLogin() );

    }
}